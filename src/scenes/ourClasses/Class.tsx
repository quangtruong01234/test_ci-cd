type Props = {
  name: string;
  description?: string;
  image: string;
};

const Class = ({ name, description, image }: Props) => {
  const overlayStyles = `md:p-5 absolute z-30 flex
  md:h-[380px] md:w-[450px] w-[350px] h-[265px]  flex-col items-center justify-center
  whitespace-normal bg-primary-500 text-center text-white
  opacity-0 transition duration-500 hover:opacity-90`;
  return (
    <li className="relative mx-5 inline-block md:h-[380px] md:w-[450px] w-[350px] h-[200px]">
      <div className={overlayStyles}>
        <p className="text-2xl">{name}</p>
        <p className="md:mt-5">{description}</p>
      </div>
      <img alt={`${image}`} src={image} />
    </li>
  )
}

export default Class